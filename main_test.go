package main

import (
	"encoding/json"
	"fmt"
	"github.com/gdgvda/cron"
	"github.com/go-test/deep"
	"go.uber.org/zap/zaptest"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func TestSearchReddit(t *testing.T) {
	log := zaptest.NewLogger(t).Sugar()
	jsonBytes, _ := os.ReadFile("test_data/search_result.json")

	tests := []struct {
		name         string
		status       int
		response     []byte
		requestError bool
	}{
		{
			name:     "Success",
			status:   200,
			response: jsonBytes,
		},
		{
			name:     "Success",
			status:   200,
			response: nil,
		},
		{
			name:         "Connection issue",
			requestError: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				// Reddit is a stickler for custom user agents to avoid getting 429 errors
				userAgent := r.Header.Get("User-Agent")
				expectedUserAgent := "reddit-f1-scanner"
				if userAgent != expectedUserAgent {
					t.Errorf("Expecting User-Agent %s, got %s", expectedUserAgent, userAgent)
				}
				if r.URL.Path != "/search.json" {
					t.Errorf("Expected to request '/search.json', got: %s", r.URL.Path)
				}
				w.WriteHeader(tt.status)
				w.Write(tt.response)
			}))
			if tt.requestError {
				server.Close()
			} else {
				defer server.Close()
			}

			data, err := searchReddit(*log, server.URL)
			if !tt.requestError && err != nil {
				t.Errorf("Not expecting an error but got %s", err)
			}
			t.Logf("Got %s\n", string(data))
		})
	}
}

// TestUnmarshalRedditResults tests the unmarshalling of a JSON document representing Reddit search results into a slice
// of rounds. These tests cover the high-level unmarshalling; specific round parsing is covered by TestParseRedditPost.
func TestUnmarshalRedditResults(t *testing.T) {
	log := zaptest.NewLogger(t).Sugar()
	sampleJson, _ := os.ReadFile("test_data/search_result.json")
	appConfig := config{ScanHistory: ScanYear}

	// Parse the sample data so we can derive some expected values
	sampleRounds, err := unmarshalRedditResults(log, appConfig, sampleJson)
	if err != nil {
		t.Errorf("Failed to parse sample data %e", err)
	}

	tests := []struct {
		name       string
		appConfig  config
		json       []byte
		wantRounds int
		wantErr    bool
	}{
		{
			name:      "Nil JSON returns nil",
			appConfig: config{ScanHistory: ScanYear},
			json:      nil,
			wantErr:   true,
		},
		{
			name:       "Empty JSON returns nil",
			appConfig:  config{ScanHistory: ScanYear},
			json:       []byte("{}"),
			wantRounds: 0,
			wantErr:    false,
		},
		{
			name:      "Invalid JSON returns nil",
			appConfig: config{ScanHistory: ScanYear},
			json:      []byte("{"),
			wantErr:   true,
		},
		{
			name:       "Sample JSON returns rounds",
			appConfig:  config{ScanHistory: ScanYear},
			json:       sampleJson,
			wantRounds: 4,
			wantErr:    false,
		},
		{
			name:       "ScanHistory 'year' returns all rounds in the year",
			appConfig:  config{ScanHistory: ScanYear},
			json:       sampleJson,
			wantRounds: 4,
			wantErr:    false,
		},
		{
			name:       "ScanHistory 'weekend' returns the most recent round in the current year",
			appConfig:  config{ScanHistory: ScanWeekend},
			json:       sampleJson,
			wantRounds: 1,
			wantErr:    false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := unmarshalRedditResults(log, tt.appConfig, tt.json)
			if tt.wantErr {
				if err == nil {
					t.Error("Expecting error, got none")
				}
			} else if tt.wantRounds != len(got) {
				t.Errorf("Expecting %d rounds to be parsed, got %d", tt.wantRounds, len(got))
			}

			// Ensure sort order
			previousRound := -1
			for _, r := range got {
				if r.Number < previousRound {
					t.Errorf("Sort order incorrect, expected round %d to be after round %d", r.Number, previousRound)
				}
				previousRound = r.Number
			}

			// Ensure ScanWeekend chose the correct round
			if tt.appConfig.ScanHistory == ScanWeekend {
				wantRound := sampleRounds[len(sampleRounds)-1].Number
				gotRound := got[0].Number
				if gotRound != wantRound {
					t.Errorf("Expecting current weekend to be round %d, got %d", wantRound, gotRound)
				}
			}
		})
	}
}

// TestParseRedditPost covers the translation of title and body strings of a Reddit post into a round.
func TestParseRedditPost(t *testing.T) {
	log := zaptest.NewLogger(t).Sugar()
	var r redditSearchResult
	sampleJson, _ := os.ReadFile("test_data/search_result.json")
	err := json.Unmarshal(sampleJson, &r)
	if err != nil {
		t.Errorf("Failed to load sample data: %s", err)
	}
	fmt.Printf("%s\n", r.Data.Children[0].Data.Selftext)

	tests := []struct {
		name    string
		title   string
		body    string
		want    round
		wantErr bool
	}{
		{
			name:    "Empty title",
			title:   "",
			wantErr: true,
		},
		{
			name:    "Non-conforming title",
			title:   "Random reddit post that doesn't match our expected format",
			wantErr: true,
		},
		{
			name:  "F1 Preseason testing",
			title: "F1 2019. Preseason Testing. Sky Sports F1 HD. 1080P",
			// Grab the first result's body text as an example
			body: r.Data.Children[0].Data.Selftext,
			want: round{Year: 2019, Number: 0, Title: "Pre-Season Testing", Resolution: 1080, MagnetLink: "magnet:?xt=urn:btih:b3cf231385bd1f928ea92cee4a8a57eca055a9a3&dn=Formula%201.%202023.%20R02.%20Saudi%20Arabian.%20SkyF1HD.%201080P&tr=udp%3A%2F%2Fopen.stealth.si%3A80%2Fannounce&tr=udp%3A%2F%2Fexodus.desync.com%3A6969%2Fannounce&tr=udp%3A%2F%2Ftracker.cyberia.is%3A6969%2Fannounce&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337%2Fannounce&tr=udp%3A%2F%2Ftracker.torrent.eu.org%3A451%2Fannounce&tr=udp%3A%2F%2Fexplodie.org%3A6969%2Fannounce&tr=udp%3A%2F%2Ftracker.birkenwald.de%3A6969%2Fannounce&tr=udp%3A%2F%2Ftracker.moeking.me%3A6969%2Fannounce&tr=udp%3A%2F%2Fipv4.tracker.harry.lu%3A80%2Fannounce&tr=udp%3A%2F%2F9.rarbg.me%3A2970%2Fannounce"},
		},
		{
			name:  "F1. Preseason testing",
			title: "F1. 2021. Preseason Testing. Sky Sports F1 HD",
			want:  round{Year: 2021, Number: 0, Resolution: 0, Title: "Pre-Season Testing"},
		},
		{
			name:  "F1. ... Grand Prix suffix",
			title: "F1. 2021. R18. Mexican Grand Prix. Weekend On Sky F1 HD. 1080P",
			want:  round{Year: 2021, Number: 18, Resolution: 1080, Title: "Mexican"},
		},
		{
			name:  "F1. ... Testing",
			title: "F1. 2022. Testing. Bahrain. SkyF1HD. 1080P",
			want:  round{Year: 2022, Number: 0, Resolution: 1080, Title: "Pre-Season Testing"},
		},
		{
			name:  "Formula 1. ... Weekend suffix",
			title: "Formula 1. 2022. R22. Abu Dhabi Weekend. SkyF1HD. 1080P",
			want:  round{Year: 2022, Number: 22, Resolution: 1080, Title: "Abu Dhabi"},
		},
		{
			name:  "Formula 1. ... Pre Season Testing",
			title: "Formula 1. 2023. Pre Season Testing. Bahrain. SkyF1HD. 1080P",
			want:  round{Year: 2023, Number: 0, Resolution: 1080, Title: "Pre-Season Testing"},
		},
		{
			name:  "Formula 1. ... no GP/Weekend suffix",
			title: "Formula 1. 2023. R01. Bahrain. SkyF1HD. 1080P",
			want:  round{Year: 2023, Number: 1, Resolution: 1080, Title: "Bahrain"},
		},
		{
			name:  "Formula 1., dots and spaces, no GP/Weekend suffix",
			title: "Formula 1. 2023. R02. Saudi Arabian. SkyF1HD. 1080P",
			want:  round{Year: 2023, Number: 2, Resolution: 1080, Title: "Saudi Arabian"},
		},
		{
			name:  "F1, dots no spaces, Weekend suffix",
			title: "F1.2023.Round.23.Abu.Dhabi.Weekend.SkyF1.1080P",
			want:  round{Year: 2023, Number: 23, Resolution: 1080, Title: "Abu Dhabi"},
		},
		{
			name:  "Complete round parsing, 1080P",
			title: "Formula 1. 2023. R02. Saudi Arabian. SkyF1HD. 1080P",
			want:  round{Year: 2023, Number: 2, Resolution: 1080, Title: "Saudi Arabian"},
		},
		{
			name:  "F1, dots no spaces, 2160P",
			title: "F1.2024.R09.Canadian.Grand.Prix.SkyUHD.2160P",
			want:  round{Year: 2024, Number: 9, Resolution: 2160, Title: "Canadian"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := parseRedditPost(tt.title, tt.body, log)
			if tt.wantErr != (err != nil) {
				t.Errorf("Want error: %t, got: %s", tt.wantErr, err)
			} else {
				diff := deep.Equal(got, tt.want)
				if diff != nil {
					t.Errorf("Parsing '%s': %#v", tt.title, diff)
				}
			}
		})
	}
}

// TestParseRedditPost covers the translation of title and body strings of a Reddit post into a round.
func TestSelectDesiredResolution(t *testing.T) {
	log := zaptest.NewLogger(t).Sugar()
	tests := []struct {
		name              string
		rounds            []round
		desiredResolution int
		want              []round
	}{
		{
			name:              "Want a specific resolution",
			desiredResolution: 1080,
			rounds: []round{
				{Year: 2024, Number: 7, Resolution: 1080, Title: "Monaco"},
				{Year: 2024, Number: 7, Resolution: 2080, Title: "Monaco"},
				{Year: 2024, Number: 8, Resolution: 1080, Title: "Canadian"},
				{Year: 2024, Number: 8, Resolution: 2080, Title: "Canadian"},
			},
			want: []round{
				{Year: 2024, Number: 7, Resolution: 1080, Title: "Monaco"},
				{Year: 2024, Number: 8, Resolution: 1080, Title: "Canadian"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := selectDesiredResolution(*log, tt.desiredResolution, tt.rounds)
			diff := deep.Equal(got, tt.want)
			if diff != nil {
				t.Errorf("got: %v\nwant: %v\ndiff: %#v\n", got, tt.want, diff)
			}
		})
	}
}

// TestBtihFromMagnetLink tests the extraction of the BitTorrent info hash from a magnet link
func TestBtihFromMagnetLink(t *testing.T) {
	tests := []struct {
		name    string
		magnet  string
		want    string
		wantErr bool
	}{
		{
			name:   "Basic extraction from a magnet link",
			magnet: "magnet:?xt=urn:btih:138e721dd576547ce9b9bd379a1a9a42f3dc524f&dn=Formula%201.%202023.%20R03.%20Australian.%20SkyF1HD.%201080P&tr=udp%3A%2F%2Fopen.stealth.si%3A80%2Fannounce",
			want:   "138e721dd576547ce9b9bd379a1a9a42f3dc524f",
		},
		{
			name:    "Magnet link without a btih hash",
			magnet:  "magnet:?dn=Formula%201.%202023.%20R03.%20Australian.%20SkyF1HD.%201080P&tr=udp%3A%2F%2Fopen.stealth.si%3A80%2Fannounce",
			want:    "",
			wantErr: true,
		},
		{
			name:    "Not a valid magnet link at all",
			magnet:  "The quick brown fox was not a magnet link",
			want:    "",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := btihFromMagnetLink(tt.magnet)
			if (err != nil) != tt.wantErr {
				t.Errorf("Wanted error: %t, got: %s", tt.wantErr, err)
			}
			if got != tt.want {
				t.Errorf("Got '%s', want '%s'", got, tt.want)
			}
		})
	}
}

func TestAddSchedules(t *testing.T) {
	log := zaptest.NewLogger(t).Sugar()
	type env struct {
		key   string
		value string
	}
	tests := []struct {
		name      string
		appConfig *config
		wantErr   bool
	}{
		{
			name:      "Default schedules",
			appConfig: defaultWithHostname(),
		},
		{
			name:      "Invalid pre-defined weekday schedule",
			appConfig: defaultWithHostname().weekdayCron("@invalid"),
			wantErr:   true,
		},
		{
			name:      "Invalid weekday schedule",
			appConfig: defaultWithHostname().weekdayCron("0 8 * * MO-TH"),
			wantErr:   true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := cron.New()
			defer c.Stop()
			err := addSchedules(*tt.appConfig, c, log)

			if (err != nil) != tt.wantErr {
				t.Errorf("Wanted error: %t, got: %s", tt.wantErr, err)
			}
		})
	}
}
