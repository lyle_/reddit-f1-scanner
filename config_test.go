package main

import (
	"github.com/go-test/deep"
	"os"
	"testing"
)

var testHostName = "fake.url/foo"

func defaultWithHostname() *config {
	return defaultConfig().transmissionHost(testHostName)
}

func Test_loadConfig(t *testing.T) {
	type env struct {
		key   string
		value string
	}
	envWithHostname := []env{{key: "TRANSMISSION_HOST", value: testHostName}}
	tests := []struct {
		name    string
		env     []env
		want    *config
		wantErr bool
	}{
		{
			name:    "Missing TRANSMISSION_HOST is an error",
			wantErr: true,
		},
		{
			name: "Load default values",
			env:  envWithHostname,
			want: defaultWithHostname(),
		},
		{
			name: "Specify LogLevel",
			env:  append(envWithHostname, env{key: "LOG_LEVEL", value: "debug"}),
			want: defaultWithHostname().logLevel("debug"),
		},
		{
			name: "Specify TransmissionHost without a port",
			env:  []env{{key: "TRANSMISSION_HOST", value: "transmission.lan"}},
			want: defaultConfig().transmissionHost("transmission.lan"),
		},
		{
			name: "Specify TransmissionHost and a port",
			env:  []env{{key: "TRANSMISSION_HOST", value: "transmission.lan:5555"}},
			want: defaultConfig().transmissionHost("transmission.lan").transmissionPort(5555),
		},
		{
			name: "Specify TransmissionDownloadDir",
			env:  append(envWithHostname, env{key: "TRANSMISSION_DOWNLOAD_DIR", value: "/tmp/f1"}),
			want: defaultWithHostname().transmissionDownloadDir("/tmp/f1"),
		},
		{
			name: "No specified ScanHistory defaults to weekend",
			env:  envWithHostname,
			want: defaultWithHostname().scanHistory(ScanWeekend),
		},
		{
			name: "ScanHistory = weekend",
			env:  append(envWithHostname, env{key: "SCAN_HISTORY", value: "weekend"}),
			want: defaultWithHostname().scanHistory(ScanWeekend),
		},
		{
			name: "ScanHistory = year",
			env:  append(envWithHostname, env{key: "SCAN_HISTORY", value: "year"}),
			want: defaultWithHostname().scanHistory(ScanYear),
		},
		{
			name: "ScanHistory is case-insensitive",
			env:  append(envWithHostname, env{key: "SCAN_HISTORY", value: "yEaR"}),
			want: defaultWithHostname().scanHistory(ScanYear),
		},
		{
			name: "WEEKDAY_CRON default",
			env:  envWithHostname,
			want: defaultWithHostname().weekdayCron(DefaultWeekdayCron),
		},
		{
			name: "WEEKEND_CRON default",
			env:  envWithHostname,
			want: defaultWithHostname().weekendCron(DefaultWeekendCron),
		},
		{
			name: "WEEKDAY_CRON setting",
			env:  append(envWithHostname, env{key: "WEEKDAY_CRON", value: "0 30 * * * *"}),
			want: defaultWithHostname().weekdayCron("0 30 * * * *"),
		},
		{
			name: "WEEKEND_CRON setting, pre-defined schedule",
			env:  append(envWithHostname, env{key: "WEEKEND_CRON", value: "@hourly"}),
			want: defaultWithHostname().weekendCron("@hourly"),
		},
		{
			name: "Specify a resolution",
			env:  append(envWithHostname, env{key: "RESOLUTION", value: "1080"}),
			want: defaultWithHostname().resolution(1080),
		},
	}

	unset := func(env []env) {
		for _, e := range env {
			os.Unsetenv(e.key)
		}
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Set environment variables
			for _, env := range tt.env {
				os.Setenv(env.key, env.value)
			}
			defer unset(tt.env)

			got, err := loadConfig()
			if (err != nil) != tt.wantErr {
				t.Errorf("loadConfig() error = %v, wantErr %v", err, tt.wantErr)
			}

			diff := deep.Equal(got, tt.want)
			if diff != nil {
				t.Errorf("loadConfig(): %v", diff)
			}
		})
	}
}
