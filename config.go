package main

import (
	"fmt"
	"go.uber.org/zap"
	"net"
	"os"
	"strconv"
	"strings"
)

type config struct {
	LogLevel                string
	TransmissionHost        string
	TransmissionPort        uint16
	TransmissionDownloadDir string
	ScanHistory             ScanHistoryTimeframe
	WeekdayCron             string
	WeekendCron             string
	DesiredResolution       int
}

// ScanHistoryTimeframe specifies how far back in time we want to scan for new rounds
type ScanHistoryTimeframe int

const (
	DefaultWeekdayCron                      = "0 8 * * MON-THU"
	DefaultWeekendCron                      = "*/5 * * * FRI,SAT,SUN"
	ScanWeekend        ScanHistoryTimeframe = iota
	ScanYear
	AllResolutions = -1
)

func defaultConfig() *config {
	return &config{
		LogLevel:                "info",
		TransmissionPort:        9091,
		TransmissionDownloadDir: "/data/completed",
		ScanHistory:             ScanWeekend,
		WeekdayCron:             DefaultWeekdayCron,
		WeekendCron:             DefaultWeekendCron,
		DesiredResolution:       AllResolutions,
	}
}

func (c *config) logLevel(level string) *config {
	c.LogLevel = level
	return c
}

func (c *config) transmissionHost(host string) *config {
	c.TransmissionHost = host
	return c
}

func (c *config) transmissionPort(port uint16) *config {
	c.TransmissionPort = port
	return c
}

func (c *config) transmissionDownloadDir(dir string) *config {
	c.TransmissionDownloadDir = dir
	return c
}

func (c *config) scanHistory(timeframe ScanHistoryTimeframe) *config {
	c.ScanHistory = timeframe
	return c
}

func (c *config) weekdayCron(spec string) *config {
	c.WeekdayCron = spec
	return c
}

func (c *config) weekendCron(spec string) *config {
	c.WeekendCron = spec
	return c
}

func (c *config) resolution(spec int) *config {
	c.DesiredResolution = spec
	return c
}

// loadConfig reads configuration parameters from the environment and generates the app's configuration
func loadConfig() (*config, error) {
	appConfig := defaultConfig()
	if value, exists := os.LookupEnv("LOG_LEVEL"); exists {
		appConfig.LogLevel = value
	}
	if value, exists := os.LookupEnv("TRANSMISSION_HOST"); exists {
		if strings.ContainsRune(value, ':') {
			if host, port, err := net.SplitHostPort(value); err == nil {
				appConfig.TransmissionHost = host
				portInt, err := strconv.Atoi(port)
				if err != nil {
					panic(err)
				}
				appConfig.TransmissionPort = uint16(portInt)
			} else {
				panic(err)
			}
		} else {
			appConfig.TransmissionHost = value
		}
	} else {
		return nil, fmt.Errorf("TRANSMISSION_HOST value is not set")
	}
	if value, exists := os.LookupEnv("TRANSMISSION_DOWNLOAD_DIR"); exists {
		appConfig.TransmissionDownloadDir = value
	}
	if value, exists := os.LookupEnv("SCAN_HISTORY"); exists {
		if strings.ToLower(value) == "year" {
			appConfig.ScanHistory = ScanYear
		} else {
			appConfig.ScanHistory = ScanWeekend
		}
	}
	if value, exists := os.LookupEnv("WEEKDAY_CRON"); exists {
		appConfig.WeekdayCron = value
	}
	if value, exists := os.LookupEnv("WEEKEND_CRON"); exists {
		appConfig.WeekendCron = value
	}
	if value, exists := os.LookupEnv("RESOLUTION"); exists {
		appConfig.DesiredResolution = parseDesiredResolution(value)
	}
	return appConfig, nil
}

func parseDesiredResolution(value string) int {
	resolution, err := strconv.Atoi(value)
	if err != nil {
		panic(err)
	}
	return resolution
}

// configureLogging sets up the logger according to configured preferences
func configureLogging(config config) *zap.SugaredLogger {
	level, err := zap.ParseAtomicLevel(config.LogLevel)
	if err != nil {
		fmt.Printf("Error parsing desired log level, defaulting to 'info': %v\n", err)
		level = zap.NewAtomicLevelAt(zap.InfoLevel)
	}

	c := zap.NewDevelopmentConfig()
	c.Level = level
	logger, err := c.Build()
	if err != nil {
		panic(err)
	}

	return logger.Sugar()
}
