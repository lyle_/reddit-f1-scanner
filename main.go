package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/gdgvda/cron"
	"github.com/hekmon/transmissionrpc/v2"
	"go.uber.org/zap"
	"html"
	"io"
	"net/http"
	"net/url"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"
)

// round is a Formula 1 event, either a Grand Prix or preseason testing.
// rounds are downloaded with magnet links that get updated as more sessions become available.
type round struct {
	Number     int    // 0 for preseason testing, regular numbering for race weekends
	Title      string // "Bahrain", "Saudi Arabian". Read as implicitly followed by "Grand Prix" or "GP"
	Year       int    // 2023
	Resolution int    // 1080, 2160
	MagnetLink string // the magnet link a BitTorrent client can use to download content for the round
}

func main() {
	appConfig, err := loadConfig()
	if err != nil {
		panic(err)
	}
	log := configureLogging(*appConfig)

	oneshot := flag.Bool("oneshot", false, "run once and exit")
	flag.Parse()
	if *oneshot {
		log.Info("Running a one-shot search")
		lookForNewLinks(*appConfig, log)
	} else {
		c := cron.New()
		err = addSchedules(*appConfig, c, log)
		if err != nil {
			panic(err)
		}
		defer c.Stop()
		log.Info("Running scheduled searches")
		c.Run()
	}
}

// addSchedules configures cron schedules to check Reddit for new links
func addSchedules(appConfig config, c *cron.Cron, log *zap.SugaredLogger) error {
	_, err := c.Add(appConfig.WeekdayCron, func() {
		lookForNewLinks(appConfig, log.With(
			zap.String("schedule", "weekday"),
			zap.String("cron", appConfig.WeekdayCron),
		))
	})
	if err != nil {
		return fmt.Errorf("adding weekday schedule: %w", err)
	}

	_, err = c.Add(appConfig.WeekendCron, func() {
		lookForNewLinks(appConfig, log.With(
			zap.String("schedule", "weekend"),
			zap.String("cron", appConfig.WeekendCron),
		))
	})
	if err != nil {
		return fmt.Errorf("adding weekend schedule: %w", err)
	}

	return nil
}

// lookForNewLinks queries Reddit and sends any new torrents to Transmission
func lookForNewLinks(appConfig config, log *zap.SugaredLogger) {
	log.Info("Looking for new links")

	// Get current season links from Reddit
	response, err := searchReddit(*log, "https://www.reddit.com")
	if err != nil {
		log.Error(err)
	}
	rounds, err := unmarshalRedditResults(log, appConfig, response)
	if err != nil {
		log.Error(err)
	}
	log.Infof("Processed %d rounds from Reddit", len(rounds))

	if appConfig.DesiredResolution != AllResolutions {
		// Filter out rounds with desired resolution
		rounds = selectDesiredResolution(*log, appConfig.DesiredResolution, rounds)
		log.Infof("Selected %d rounds for desired resolution '%d'", len(rounds), appConfig.DesiredResolution)
	}

	// Get torrents the Transmission server knows about
	transmission, err := transmissionrpc.New(appConfig.TransmissionHost, "", "", nil)
	if err != nil {
		log.Errorf("Creating Transmission RPC client: %v", err)
		return
	}
	ok, serverVersion, serverMinimumVersion, err := transmission.RPCVersion(context.TODO())
	if err != nil {
		log.Errorf("Getting RPC version of Transmission server: %v", err)
		return
	}
	if !ok {
		log.Errorf("Remote transmission RPC version (v%d) is incompatible with the transmission library (v%d): remote needs at least v%d",
			serverVersion, transmissionrpc.RPCVersion, serverMinimumVersion)
		return
	}
	torrents, err := transmission.TorrentGetAll(context.TODO())
	if err != nil {
		log.Errorf("Getting existing torrents: %v", err)
		return
	}
	knownMagnets := make(map[string]bool)
	for _, t := range torrents {
		log.Debugf("%s: magnet link %s", *t.Name, *t.MagnetLink)
		hash, err := btihFromMagnetLink(*t.MagnetLink)
		if err != nil {
			log.Errorf("Extracting hash from magnet link '%s': %s", *t.MagnetLink, err)
		} else {
			knownMagnets[hash] = true
		}
	}

	for _, round := range rounds {
		hash, err := btihFromMagnetLink(round.MagnetLink)
		if err != nil {
			log.Errorf("Extracting hash from magnet link '%s': %s", round.MagnetLink, err)
			continue
		}
		if !knownMagnets[hash] {
			log.Debugf("Transmission server doesn't know about %s (%s)", round.Title, round.MagnetLink)
			torrent, err := transmission.TorrentAdd(context.TODO(), transmissionrpc.TorrentAddPayload{
				Filename:    &round.MagnetLink,
				DownloadDir: &appConfig.TransmissionDownloadDir,
			})
			if err != nil {
				log.Errorf("Adding torrent: %s", err)
			} else {
				log.Infof("Added torrent ID: %d, Name: %s, Hash: %s", *torrent.ID, *torrent.Name, *torrent.HashString)
			}
		} else {
			log.Infof("Transmission server knows about %s (%s)", round.Title, round.MagnetLink)
		}
	}
	log.Info("Processing finished")
}

// selectDesiredResolution examines rounds and returns only those matching the desired resolution
func selectDesiredResolution(log zap.SugaredLogger, resolution int, allRounds []round) []round {
	var filtered []round
	for _, r := range allRounds {
		if r.Resolution == resolution {
			filtered = append(filtered, r)
		} else {
			log.Debugf("Round does not meet desired resolution criteria (%d): %#v\n", resolution, r)
		}
	}
	return filtered
}

// searchReddit queries Reddit for the latest race links in current year.
// It returns a JSON payload representing search results.
func searchReddit(log zap.SugaredLogger, baseUrl string) (jsonResponse []byte, err error) {
	currentYear, _, _ := time.Now().Date()
	query := url.QueryEscape(fmt.Sprintf("subreddit:motorsportsreplays author:egortech title:(\"F1. %d.\" OR \"Formula 1 %d.\")", currentYear, currentYear))
	redditSearchUrl := fmt.Sprintf("%s/search.json?q=%s", baseUrl, query)
	log.Debugf("Searching Reddit for %d races: %s", currentYear, redditSearchUrl)

	request, _ := http.NewRequest(http.MethodGet, redditSearchUrl, nil)
	request.Header.Set("User-Agent", "reddit-f1-scanner")
	client := &http.Client{}
	resp, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("searching Reddit for %d races: %w", currentYear, err)
	}
	defer resp.Body.Close()

	log.Debugf("StatusCode: %d, ContentLength %d bytes", resp.StatusCode, resp.ContentLength)
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("searching Reddit for %d races, reading response code: %w", currentYear, err)
	}
	return body, nil
}

// redditSearchResult is a struct representing the JSON structure of Reddit search results.
type redditSearchResult struct {
	Data struct {
		Children []struct {
			Data struct {
				Title    string `json:"title"`
				Selftext string `json:"selftext"`
			} `json:"data"`
		} `json:"children"`
	} `json:"data"`
}

// unmarshalRedditResults parses a JSON object representing Reddit search results and returns a slice of
// the latest rounds returned (sorted by round number) or nil if none were found.
func unmarshalRedditResults(log *zap.SugaredLogger, appConfig config, jsonResults []byte) ([]round, error) {
	var r redditSearchResult
	err := json.Unmarshal(jsonResults, &r)
	if err != nil {
		return nil, err
	}

	var rounds []round
	for _, r := range r.Data.Children {
		round, err := parseRedditPost(r.Data.Title, r.Data.Selftext, log)
		if err != nil {
			return nil, err
		}
		log.Debugf("Unmarshalled %d - R%d - %s", round.Year, round.Number, round.Title)
		rounds = append(rounds, round)
	}

	// Sort by round number
	sort.Slice(rounds, func(i, j int) bool {
		return rounds[i].Number < rounds[j].Number
	})

	if rounds != nil && appConfig.ScanHistory == ScanWeekend {
		return rounds[len(rounds)-1:], nil
	}
	return rounds, nil
}

// parseRedditPost examines the title and body for a single Reddit post and extracts the details necessary to return a round.
func parseRedditPost(title, body string, log *zap.SugaredLogger) (round, error) {
	round := round{}

	// Regex can be tested at https://regexr.com/81ecf
	re := `(?i)(?:Formula 1|F1)[\. ]+(?P<year>\d{4})[\. ]+(?P<number>(R\d{2}|Round\.\d{2}|.*testing))[\. ]+(?P<title>.*)(?:Sky)(?:.*)[\. ](?P<resolution>.*P)?`
	r := regexp.MustCompile(re)
	matches := r.FindStringSubmatch(title)
	if len(matches) == 0 {
		return round, fmt.Errorf("parsing title '%s'; got %d matches", title, len(matches))
	}

	year, err := strconv.Atoi(matches[1])
	if err != nil {
		return round, fmt.Errorf("parsing year from title '%s': %w", title, err)
	}
	round.Year = year

	if strings.Contains(strings.ToLower(matches[2]), "testing") {
		round.Number = 0
		round.Title = "Pre-Season Testing"
	} else {
		// Trim the leading characters from "R01" or "Round.01" and convert to int
		index := strings.IndexAny(matches[2], "0123456789")
		if index < 0 {
			return round, fmt.Errorf("no digit found in round substring '%s'", matches[2])
		}
		number, err := strconv.Atoi(matches[2][index:])
		if err != nil {
			return round, fmt.Errorf("parsing round number from title '%s': %w", title, err)
		}
		round.Number = number

		round.Title = matches[4]
		// There's probably something I can do with the current regex to capture titles perfectly, but as it is we sometimes
		// see "Abu Dhabi Weekend" or "Abu Dhabi Grand Prix". I'd love to figure out the ideal regex, but given the difficulty
		// I'm having I'll concede for now with some manual trimming.
		index = strings.Index(round.Title, "Weekend")
		if index >= 0 {
			round.Title = strings.TrimSuffix(round.Title, round.Title[index:])
		}
		index = strings.Index(round.Title, "Grand")
		if index >= 0 {
			round.Title = strings.TrimSuffix(round.Title, round.Title[index:])
		}
		round.Title = strings.ReplaceAll(round.Title, ".", " ")
		round.Title = strings.TrimSpace(round.Title)

	}

	resolutionStr := strings.TrimSuffix(matches[5], "P")
	resolution, err := strconv.Atoi(resolutionStr)
	if err != nil {
		log.Warnf("Could not determine resolution: %s\n", err)
	} else {
		round.Resolution = resolution
	}

	r = regexp.MustCompile(`(magnet:[\S]+)`)
	matches = r.FindStringSubmatch(body)
	if matches != nil {
		round.MagnetLink = html.UnescapeString(matches[0])
	}

	return round, nil
}

// bthihFromMagnetLink takes a magnet link and returns the BitTorrent info hash (BTIH) from it.
// Magnet links referencing the same exact content can vary based on other metadata such as tracker URLs, so
// for the purposes of comparing two links we only care about the hash of the downloadable content.
// See: https://en.wikipedia.org/wiki/Magnet_URI_scheme
func btihFromMagnetLink(magnetLink string) (string, error) {
	re := `magnet:\?.*xt=urn:btih:(?P<btih>[a-f0-9]{40})`
	r := regexp.MustCompile(re)
	matches := r.FindStringSubmatch(magnetLink)
	if len(matches) != 2 {
		return "", fmt.Errorf("matching xt=urn:btih:... hashes, got %d matches", len(matches))
	}
	return matches[1], nil
}
