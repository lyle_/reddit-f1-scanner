FROM golang:latest as build

WORKDIR /app
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY *.go ./
RUN CGO_ENABLED=0 GOOS=linux go build -o reddit-f1-scanner

FROM debian:bullseye-slim

RUN \
  echo "**** install packages ****" && \
  apt-get update && \
  apt-get install -y \
    ca-certificates \
    passwd && \
  echo "**** add abc user ****" && \
  useradd --user-group --home-dir /data --shell /bin/false abc && \
  usermod --groups users abc && \
  echo "**** cleanup ****" && \
  apt-get autoremove && \
  apt-get clean

USER abc

VOLUME /data

WORKDIR /app
COPY --from=build --chown=abc:users /app/reddit-f1-scanner .

ENTRYPOINT ["/app/reddit-f1-scanner"]
