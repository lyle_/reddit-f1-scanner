module reddit-f1-scanner

go 1.19

require (
	github.com/gdgvda/cron v0.2.0
	github.com/go-test/deep v1.1.0
	github.com/hekmon/transmissionrpc/v2 v2.0.1
	go.uber.org/zap v1.27.0
)

require (
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hekmon/cunits/v2 v2.1.0 // indirect
	go.uber.org/multierr v1.11.0 // indirect
)
