# Reddit F1 Scanner

Searches for Formula 1 torrents posted on [r/MotorsportsReplays](https://www.reddit.com/r/MotorsportsReplays/) and sends them to [Transmission](https://transmissionbt.com/ for downloading.

In particular, it examines posts by [u/egortech](https://www.reddit.com/user/egortech/) since they are currently a very reliable uploader who names posts consistently, which streamlines automation.

Searches are run on two different, configurable schedules: *midweek* and *weekend*.
During the middle of the week it's unlikely there will be any new files available (maybe the rare update or correction) so we probably don't need to check very often.
On the weekends, when you're on the edge of your seat waiting for the new session to be published, a more aggressive schedule can be set.

## Configuration

The following environment variables are used to configure the application.
Variables without a default value are **required**.

| Variable | Default | Notes |
|----------|---------|-------|
| TRANSMISSION_HOST | | Hostname without "http(s):" prefix. If no port number is specified, the default is 9091. Examples: `transmission.lan`, `transmission.lan:5555` |
| TRANSMISSION_DOWNLOAD_DIR | `/data/completed` | Directory to store completed downloads on the transmission host | `/data/completed/f1` |
| LOG_LEVEL | `info` | Log level string. `debug`, `info`, `warn`, or `error` |
| SCAN_HISTORY | `weekend` | How many rounds to scan for, either `weekend` to only grab the latest round or `year` to get all rounds in the current calendar year. |
| WEEKDAY_CRON | `0 8 * * MON-THU` (8am Monday through Thursday) | [Cron expression](https://en.wikipedia.org/wiki/Cron) for less-frequent updates, such as the middle of the week when nothing is happening. |
| WEEKEND_CRON | `*/5 * * * FRI,SAT,SUN` (every 5 minutes Friday through Sunday). | [Cron expression](https://en.wikipedia.org/wiki/Cron) for more frequent updates when you want new sessions ASAP. |
| RESOLUTION | | Selects a particular resolution to download (`1080`, `2160`). Defaults to downloading all available resolutions. |

## Running

### Docker (-compose)

For the latest version, use `lyleh/reddit-f1-scanner:latest` Docker Hub: https://hub.docker.com/r/lyleh/reddit-f1-scanner

Consult the included `docker-compose.yml` for an example of using [docker-compose](https://docs.docker.com/compose/) to
run this tool together with an instance of [docker-transmission-openvpn](https://haugene.github.io/docker-transmission-openvpn/).

To run this example, you'll need access to a supported OpenVPN provider (see
[docker-transmission-openvpn providers](https://haugene.github.io/docker-transmission-openvpn/supported-providers/)).
Populate a `.env` file to specify the referenced `OPENVPN_*` configuration variables and run the following (some tweaking may be required to
run the containers as your desired UID):

```sh
mkdir data_reddit_f1 # Create data directory to mount into container
docker-compose up --build # add --detach to keep containers running in the background
```

### Direct execution

`go run main.go`

### One-shot exectuion

Normally, the app runs on a schedule.
To simply execute a single scan and exit, invoke with the `-oneshot` argument.

## References

* [r/MotorsportsReplays](https://www.reddit.com/r/MotorsportsReplays/)
* Sample Reddit search parameters (adjust for year): `subreddit:motorsportsreplays author:egortech title:("F1. 2023." OR "Formula 1 2023.")`
* [Transmission RPC specification](https://github.com/transmission/transmission/blob/main/docs/rpc-spec.md)
* [Transmission configuration options](https://github.com/transmission/transmission/blob/main/docs/Editing-Configuration-Files.md)
