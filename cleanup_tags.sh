#!/bin/bash
# Every CI pipeline will create and tag a docker image to scan for vulnerabilities.
# Not every commit/pipeline will result in a merge to main, so the image creation job
# tracks images created on non-default branches in the `scan_tags` cache file and this
# script removes those no-longer-used images.
#
# The script expects the following environment variables to be set:
#
# DOCKER_REGISTRY_USER
# DOCKER_REGISTRY_PASSWORD

cat scan_tags | while read tag || [[ -n $tag ]];
do
	echo "Requesting JWT token from Docker Hub"
	TOKEN=$(curl \
		--silent \
		-X POST \
		--header "Content-Type: application/json" \
		--data "{\"username\": \"$DOCKER_REGISTRY_USER\", \"password\": \"$DOCKER_REGISTRY_PASSWORD\"}" \
		https://hub.docker.com/v2/users/login/ \
		| jq -r .token)

	echo "Deleting tag $tag"
	curl -X DELETE \
		--header "Authorization: JWT ${TOKEN}" \
		"https://hub.docker.com/v2/repositories/lyleh/reddit-f1-scanner/tags/$tag/"
	printf "\n\n"
done

# Truncate the file
> scan_tags
